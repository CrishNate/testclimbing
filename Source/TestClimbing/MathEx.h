﻿#pragma once

#include "TestClimbing.h"

struct FMathEx
{
    static float AngleBetweenVectors(const FVector& Vec1, const FVector& Vec2);

    inline static float SignedSqrt(float Value) { return FMath::Sign(Value) * FMath::Sqrt(FMath::Abs(Value)); }
    FORCEINLINE static float SignedLoge(float Value) { return FMath::Sign(Value) * FMath::Loge(FMath::Abs(Value) + 1.0f); }
    FORCEINLINE static float PseudoSignedLoge(float Value) { Value *= 0.5f; return FMath::Loge(Value + FMath::Sqrt(Value * Value + 1.0f)); }
};
