// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FClimbingAction.generated.h"


USTRUCT(Blueprintable)
struct FClimbingAction
{
	GENERATED_BODY()

	
public:
	FClimbingAction()
	: StartLocation(ForceInit)
    , TargetLocation(ForceInit)
    , TargetFacingDirection(ForceInit)
    , StartVelocity(ForceInit)
    , Duration(0)
	{ }

	FClimbingAction(const FVector& Start, const FVector& Target, const FVector& InTargetFacingDirection,
		float InDuration = 0.f, bool IsFinish = false);


public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FVector StartLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FVector TargetLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FVector StartVelocity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector TargetFacingDirection;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Duration;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    bool bFinish;
};
