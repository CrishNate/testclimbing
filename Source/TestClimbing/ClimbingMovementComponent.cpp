// Fill out your copyright notice in the Description page of Project Settings.


#include "ClimbingMovementComponent.h"


#include "LedgeActor.h"
#include "TestClimbingCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Engine/World.h"
#include "Kismet/KismetSystemLibrary.h"
#include "MathEx.h"
#include "Net/UnrealNetwork.h"

#include "DrawDebugHelpers.h"


UClimbingMovementComponent::UClimbingMovementComponent(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
	SetIsReplicatedByDefault(true);
}

void UClimbingMovementComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UClimbingMovementComponent, bIsHanging);
    DOREPLIFETIME(UClimbingMovementComponent, bIsLeaping);
}

void UClimbingMovementComponent::SetUpdatedComponent(USceneComponent* NewUpdatedComponent)
{
    Super::SetUpdatedComponent(NewUpdatedComponent);
	
    CharOwner = Cast<ATestClimbingCharacter>(CharacterOwner);

    if (CharacterOwner && NewUpdatedComponent->IsA<UCapsuleComponent>())
    {
        ACharacter* DefaultCharacter = CharacterOwner->GetClass()->GetDefaultObject<ACharacter>();
        DefaultCharacterCapsuleRadius = DefaultCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius();
		DefaultCharacterCapsuleHalfHeight = DefaultCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
    }
}

bool UClimbingMovementComponent::CheckCanMoveToPoint(const FVector& From, const FVector& NormalFrom, const FVector& To, const FVector& NormalTo)
{
    TArray<AActor*> IgnoreActors;
    GetOwner()->GetAllChildActors(IgnoreActors, true);
    IgnoreActors.Add(GetOwner());
    
    FHitResult HitResult;
    bool TraceHit = UKismetSystemLibrary::LineTraceSingle(GetWorld(), From + NormalFrom * DefaultCharacterCapsuleRadius,
                                                          To + NormalTo * DefaultCharacterCapsuleRadius,
                                                          ETraceTypeQuery::TraceTypeQuery1, false, IgnoreActors,
                                                          EDrawDebugTrace::None, HitResult, true);

    return !TraceHit;
}

void UClimbingMovementComponent::InitNewMove()
{
    bIsLeaping = true;
    Velocity = ClimbingActions[0].StartVelocity;
    SetMovementMode(MOVE_Custom, uint8(ECustomMovementType::MoveTask));

    bOrientRotationToMovement = false;
}

float UClimbingMovementComponent::GetCurrentTimeStampForMoveTask() const
{
    float TimeStamp = GetWorld()->GetTimeSeconds();

    if (GetNetMode() != NM_Standalone)
    {
        if (GetOwnerRole() == ROLE_Authority)
        {
            TimeStamp = GetPredictionData_Server_Character()->CurrentClientTimeStamp;
        }
        else
        {
            TimeStamp = GetPredictionData_Client_Character()->CurrentTimeStamp;
        }
    }

    return TimeStamp;
}

bool UClimbingMovementComponent::TryHang()
{
    if (ClimbingActions.Num() > 0)
        return false;
    
    if (GetCurrentTimeStampForMoveTask() < LastHangingTime)
        return false;

    if (bIsHanging)
        return false;

    const FVector Location = UpdatedComponent->GetComponentLocation() + UpdatedComponent->GetForwardVector() * DefaultCharacterCapsuleRadius
                        + FVector::UpVector * DefaultCharacterCapsuleHalfHeight;

    TArray<AActor*> Results;
    TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
    
    UKismetSystemLibrary::SphereOverlapActors(GetWorld(), Location, ClimbRadius, ObjectTypes, ALedgeActor::StaticClass(), TArray<AActor*>(), Results);

    FTransform ClosestTransform = FTransform::Identity;
    float CurrentClosestDistance = -1;

    for (AActor* Actor : Results)
    {
        ALedgeActor* LedgeActor = Cast<ALedgeActor>(Actor);

        for (FTransform ClimbPoint : LedgeActor->GetClimbPoints())
        {
            float Distance = (ClimbPoint.GetLocation() - Location).SizeSquared();
            if (!CheckCanMoveToPoint(Location, -UpdatedComponent->GetForwardVector(),
                                     ClimbPoint.GetLocation(), -ClimbPoint.GetRotation().GetForwardVector()))
                continue;

            if (CurrentClosestDistance > Distance || CurrentClosestDistance < 0)
            {
                CurrentClosestDistance = Distance;
                ClosestTransform = ClimbPoint;
            }
        }
    }

    if (CurrentClosestDistance > 0)
    {
        FClimbingAction ClimbingAction(Location, ClosestTransform.GetLocation(), ClosestTransform.GetRotation().GetForwardVector(), 0.1f);

        ClimbingActions.Add(ClimbingAction);
        InitNewMove();

        bIsHanging = true;
        if (GetOwnerRole() == ROLE_AutonomousProxy)
        {
            ServerInitNewMove(ClimbingActions);
        }

        return true;
    }
    
    return false;
}

void UClimbingMovementComponent::ServerInitNewMove_Implementation(const TArray<FClimbingAction>& ClientClimbingActions)
{
    ClimbingActions = ClientClimbingActions;
    bIsHanging = true;
    
    InitNewMove();
}

void UClimbingMovementComponent::ServerReleaseHang_Implementation()
{
    ReleaseHang();
}


bool UClimbingMovementComponent::LeapToDirection(FVector Direction)
{
    if (ClimbingActions.Num() > 0)
        return false;
    
    if (GetCurrentTimeStampForMoveTask() < LastHangingTime)
        return false;

    if (!bIsHanging)
        return false;

    const FVector Location = UpdatedComponent->GetComponentLocation() + UpdatedComponent->GetForwardVector() * DefaultCharacterCapsuleRadius
                                + FVector::UpVector * DefaultCharacterCapsuleHalfHeight;

    float SelectedLeapRadius = GetCharacterOwner()->bPressedJump ? LeapRadius : ClimbRadius;
        
    TArray<AActor*> Results;
    TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
    UKismetSystemLibrary::SphereOverlapActors(GetWorld(), Location, SelectedLeapRadius, ObjectTypes,
                                              ALedgeActor::StaticClass(), TArray<AActor*>(), Results);

    const float InnerIgnoreRadius = 10.0f;
	FTransform ClosestTransform = FTransform::Identity;
    float CurrentClosestDistance = -1;

    for (AActor* Actor : Results)
    {
        ALedgeActor* LedgeActor = Cast<ALedgeActor>(Actor);

        for (FTransform ClimbPoint : LedgeActor->GetClimbPoints())
        {
            FVector TargetDirection = ClimbPoint.GetLocation() - Location;
            float Distance = TargetDirection.SizeSquared();

            if (FMathEx::AngleBetweenVectors(TargetDirection, Direction) > ClimbFOV || Distance < InnerIgnoreRadius ||
                !CheckCanMoveToPoint(Location, -UpdatedComponent->GetForwardVector(),
                                     ClimbPoint.GetLocation(), -ClimbPoint.GetRotation().GetForwardVector()))
                continue;

            if (CurrentClosestDistance > Distance || CurrentClosestDistance < 0)
            {
                CurrentClosestDistance = Distance;
                ClosestTransform = ClimbPoint;
            }
        }
    }
    
    if (CurrentClosestDistance > 0)
    {
        FClimbingAction ClimbingAction(Location, ClosestTransform.GetLocation(), ClosestTransform.GetRotation().GetForwardVector(), LeapDuration);
        
        ClimbingActions.Add(ClimbingAction);
        InitNewMove();

        if (GetOwnerRole() == ROLE_AutonomousProxy)
        {
            ServerInitNewMove(ClimbingActions);
        }
        
        return true;
    }

    FHitResult HitResult;
    FVector StartTrace = Location + FVector::UpVector * DefaultCharacterCapsuleHalfHeight;
    TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjects;
    TraceObjects.Add(UEngineTypes::ConvertToObjectType(ECC_WorldStatic));

    TArray<AActor*> IgnoreActors;
	GetOwner()->GetAllChildActors(IgnoreActors, true);
    IgnoreActors.Add(GetOwner());

    bool TraceHit = UKismetSystemLibrary::SphereTraceSingle(GetWorld(), StartTrace,
                                                            (FVector::DownVector * DefaultCharacterCapsuleHalfHeight) +
                                                            StartTrace, DefaultCharacterCapsuleRadius,
                                                            ETraceTypeQuery::TraceTypeQuery1, false, IgnoreActors,
                                                            EDrawDebugTrace::None, HitResult, true);
    
    if (FMathEx::AngleBetweenVectors(Direction, FVector::UpVector) < 10.0f  && TraceHit && HitResult.Distance > 0)
    {
        FClimbingAction ClimbingAction(Location, HitResult.Location +
                                       FVector::UpVector * DefaultCharacterCapsuleHalfHeight * 2.0f +
                                       UpdatedComponent->GetForwardVector() * DefaultCharacterCapsuleRadius,
                                       UpdatedComponent->GetForwardVector(), LeapDuration, true);
        
        ClimbingActions.Add(ClimbingAction);
        InitNewMove();

        if (GetOwnerRole() == ROLE_AutonomousProxy)
        {
            ServerInitNewMove(ClimbingActions);
        }
    }
    else if (GetCharacterOwner()->bPressedJump)
    {
        ReleaseHang();
    }
    
    return false;
}

void UClimbingMovementComponent::ReleaseHang()
{
    SetDefaultMovementMode();

    LastHangingTime = GetCurrentTimeStampForMoveTask() + ClimbDelay;
    
    bIsHanging = false;
    bOrientRotationToMovement = true;
    ClimbingActions.Empty();

    if (GetOwnerRole() == ROLE_AutonomousProxy)
    {
        ServerReleaseHang();
    }
}

void UClimbingMovementComponent::PhysCustom(float DeltaTime, int32 Iterations)
{
    Super::PhysCustom(DeltaTime, Iterations);
    
    const FVector PawnLocation = UpdatedComponent->GetComponentLocation();

    UpdateMoveAction(DeltaTime, Iterations);
}

void UClimbingMovementComponent::UpdateMoveAction(float DeltaTime, int32 Iterations)
{
    if (ClimbingActions.Num() == 0)
        return;
    
    if (DeltaTime < UCharacterMovementComponent::MIN_TICK_TIME)
        return;

    if (!HasValidData())
        return;

    const FVector OldLocation = UpdatedComponent->GetComponentLocation() + UpdatedComponent->GetForwardVector() * DefaultCharacterCapsuleRadius
                            + FVector::UpVector * DefaultCharacterCapsuleHalfHeight;
    
    const float TimeTick = GetSimulationTimeStep(DeltaTime, Iterations);

    // Rotate
    FQuat DesireRotation = ClimbingActions[0].TargetFacingDirection.ToOrientationQuat();
    FQuat AdjustedRotation = FMath::Lerp(UpdatedComponent->GetComponentQuat() , DesireRotation, TimeTick * 10.0f);
    
    // Move
	const FVector TargetLocation = ClimbingActions[0].TargetLocation;
    FVector Adjusted = Velocity * TimeTick;
    const FVector PredictedNewLocation = OldLocation + Adjusted;
    Adjusted += (UpdatedComponent->GetForwardVector() - AdjustedRotation.GetForwardVector()) * DefaultCharacterCapsuleRadius;

    const FVector ToTarget = TargetLocation - OldLocation;
    if (Adjusted.SizeSquared() >= ToTarget.SizeSquared() || FVector::DistSquared(PredictedNewLocation, TargetLocation) > ToTarget.SizeSquared())
    {
        Adjusted = ToTarget;
        Velocity = Adjusted / TimeTick;
        AdjustedRotation = DesireRotation;
    }
    
    MoveUpdatedComponent(Adjusted, AdjustedRotation, false);

    const float StopDistance = 5.0f;
    
    if (ToTarget.SizeSquared() < StopDistance)
    {
        OnReachedTargetLocation();
    }
}

void UClimbingMovementComponent::OnReachedTargetLocation()
{
    bIsLeaping = false;
    
    if (ClimbingActions[0].bFinish)
    {
        ReleaseHang();
    }
    else
    {
        ClimbingActions.RemoveAt(0);
    }
    
    LastHangingTime = GetCurrentTimeStampForMoveTask() + ClimbDelay;

    if (ClimbingActions.Num() > 1)
    {
        InitNewMove();

        if (GetOwnerRole() == ROLE_AutonomousProxy)
        {
            ServerInitNewMove(ClimbingActions);
        }
    }
    else
    {
        Velocity = FVector::ZeroVector;
    }
}
