// Fill out your copyright notice in the Description page of Project Settings.


#include "LedgeActor.h"
#include "Engine/StaticMeshSocket.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ALedgeActor::ALedgeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a camera boom (pulls in towards the player if there is a collision)
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;
}

void ALedgeActor::BeginPlay()
{
	Super::BeginPlay();

	for (const FName SocketName : MeshComponent->GetAllSocketNames())
	{
		const UStaticMeshSocket* Socket = MeshComponent->GetSocketByName(SocketName);
		if (FName(*Socket->Tag) == ClimbPointsTag)
		{
			ClimbPoints.Add(MeshComponent->GetSocketTransform(SocketName));
		}
	}
}

FTransform ALedgeActor::GetClosestClimbPointToPoint(FVector Point)
{
	FTransform ClosestTransform = FTransform::Identity;

	if (ClimbPoints.Num() == 1)
	{
		return ClimbPoints[0];
	}
	else
	{
		float CurrentClosestDistance = -1;

		for (FTransform ClimbPoint : ClimbPoints)
		{
			float Distance = FVector::DistSquared(Point, ClimbPoint.GetLocation());
			if (Distance > CurrentClosestDistance || CurrentClosestDistance < 0)
			{
				CurrentClosestDistance = Distance;
				ClosestTransform = ClimbPoint;
			}
		}
	}
	
	return ClosestTransform;
}

