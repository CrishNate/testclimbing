﻿#include "MathEx.h"

#include "Math/RandomStream.h"


float FMathEx::AngleBetweenVectors(const FVector& Vec1, const FVector& Vec2)
{
    float Dot = FVector::DotProduct(Vec1.GetSafeNormal(), Vec2.GetSafeNormal());
    return FMath::RadiansToDegrees(FMath::Acos(Dot));
}
