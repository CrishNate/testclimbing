// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FClimbingAction.h"
#include "TestClimbingCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "ClimbingMovementComponent.generated.h"


UENUM(BlueprintType)
enum class ECustomMovementType : uint8
{
    MoveTask = 0,
    Climbing = 1,
};


UCLASS(BlueprintType)
class TESTCLIMBING_API UClimbingMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()


public:
    UClimbingMovementComponent(const FObjectInitializer& ObjectInitializer);

    
public:
    virtual void PhysCustom(float deltaTime, int32 Iterations) override;
    virtual void SetUpdatedComponent(USceneComponent* NewUpdatedComponent) override;

    float GetCurrentTimeStampForMoveTask() const;
    bool TryHang();
    bool LeapToDirection(FVector Direction);
    void ReleaseHang();

    bool CheckCanMoveToPoint(const FVector& From, const FVector& NormalFrom, const FVector& To, const FVector& NormalTo);
    void InitNewMove();
    void UpdateMoveAction(float DeltaTime, int32 Iterations);
    void OnReachedTargetLocation();

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerInitNewMove(const TArray<FClimbingAction>& ClientClimbingActions);
    bool ServerInitNewMove_Validate(const TArray<FClimbingAction>& ClientClimbingActions) { return true; }

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerReleaseHang();
    bool ServerReleaseHang_Validate() { return true; }
    

public:
	/** Max angle in direction character can climb or leap from hanging point */
	UPROPERTY(EditDefaultsOnly, Category = "Character Movement : Climbing")
    float ClimbFOV;

	/** Time between climb or leap */
    UPROPERTY(EditDefaultsOnly, Category = "Character Movement : Climbing")
    float ClimbDelay;

	/** Range where player can climb from one ledge to another */
    UPROPERTY(EditDefaultsOnly, Category = "Character Movement : Climbing")
    float ClimbRadius;

	/** Range where player can leap from one ledge to another */
    UPROPERTY(EditDefaultsOnly, Category = "Character Movement : Climbing")
    float LeapRadius;

    UPROPERTY(EditDefaultsOnly, Category = "Character Movement : Climbing")
    float LeapDuration;

    UPROPERTY(Transient)
    float DefaultCharacterCapsuleRadius;
    
    UPROPERTY(Transient)
    float DefaultCharacterCapsuleHalfHeight;

    UPROPERTY(Replicated, BlueprintReadOnly, Category = "Character Movement : Climbing")
    bool bIsHanging;

    UPROPERTY(Replicated, BlueprintReadOnly, Category = "Character Movement : Climbing")
    bool bIsLeaping;
    

protected:
    TArray<FClimbingAction> ClimbingActions;
    ATestClimbingCharacter* CharOwner;
    float LastHangingTime;
};
