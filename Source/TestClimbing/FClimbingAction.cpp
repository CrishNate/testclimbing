// Fill out your copyright notice in the Description page of Project Settings.


#include "FClimbingAction.h"



FClimbingAction::FClimbingAction(const FVector& Start, const FVector& Target, const FVector& InTargetFacingDirection,
                                 float InDuration, bool IsFinish)
    : StartLocation(Start)
    , TargetLocation(Target)
    , TargetFacingDirection(InTargetFacingDirection)
    , Duration(InDuration)
    , bFinish(IsFinish)
{
    if (Duration > 0)
    {
        StartVelocity = (Target - Start) / Duration;
    }
}
