// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LedgeActor.generated.h"

UCLASS()
class TESTCLIMBING_API ALedgeActor : public AActor
{
	GENERATED_BODY()
	
	
public:	
	// Sets default values for this actor's properties
	ALedgeActor();

	
public:
	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
    class UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    FName ClimbPointsTag = TEXT("ClimbPoint");

	
protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
    TArray<FTransform> ClimbPoints;


public:
	virtual void BeginPlay() override;

	FTransform GetClosestClimbPointToPoint(FVector Point);
	FORCEINLINE const TArray<FTransform>& GetClimbPoints() const { return ClimbPoints; };
};
